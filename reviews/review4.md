
Project Review
=====================================================

## Overview

1) Briefly summarize the experiment in this project.
A: This experiment is basicly implemented under two different network environment(with SFQ on and off). Doing the tests to get the data packets in different bandwidth of
1Mpbs and 100Mbps with SFQ on and off. Analysing the data packets using tool Rstudio to compare the performace of using SFQ with using default scheduling. Through
compasion, they get the conclusion that SFQ does not have any better performance than the default scheduling method in network traffic control.


2) Does the project generally follow the guidelines and parameters we have
learned in class?
A: Basically it follows the guidelines and parameters we learned in class. The parameters in this experiment are different bandwidths(1Mbps and 100Mbps) and whether to
use SFQ or not.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
A:a.The goal of this experiment is to study the effects of SFQ under a situation where a small data flow is competing with a large data flow in a network. The goal is focused
on the effect of SFQ. But it is not that specific because it does not say clearly that what the bandwidths are and under which situation.
  b.It is useful and likely to get some results. The result is that SFQ does not have any better performance, that is to say SFQ does not have significant effect to the
defaul scheduling method.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
A:a.Yes, it is appropriate. The metric in this experiment is latency and parameters are different bandwidths and with SFQ on and off.
  b.Yes, the experiment designs the comparision of latency in different bandwidth and with SFQ on and off. This design can support the goal since the goal is to determine
  the effects of SFQ in different bandwidths.




3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
I think it is basically designed well since it has the comparision of small bandwidth(1m) and large bandwidth(100m) with SFQ on and off. Just as a reminder, maybe we can add
more bandwidths like 1000m and 10000m to get more results to determine the effect of SFQ.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
A:a.The experiment selects latency as the metric.
  b.It is clear to lead to correct conclusions.
  c.No, latency can be the most suitable metric for this experiment.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
A:a. Yes, the parameters is meaningful because SFQ on and off in different bandwidths forms comparision.
  b. Basicly i would say it is meaningful, but i would suggest more bandwidths instead of two(1m and 100m). We may do more tests in other bandwidths like 1000m and 10000m.


6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
The author does not address the possibility of interactions between parameters very sufficiently. Nevertheless, the author says in conclusion that there is no
evidence showing the bandwidths and variants have interaction.



7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
A:a.Yes. The comparisons are made resonably since this experiment compares the latency with SFQ on and off in different bandwidths.
  b.Yes. The comparison is appropriate because latency can inflects the scheduling efficiency with SFQ on and off under different bandwidths.
         The comparison is also realistic since the latency can be executed easily and we can use tool to analyse it.


## Communicating results


1) Do the authors report the quantitative results of their experiment?
A: Yes, the author submits the quantitative results in clear file names.


2) Is there information given about the variation and/or distribution of
experimental results?
A: Yes, the author gives many graphs drawn by Rstudio to see the variation of experimental results. The author gives brifly explation of the variation.
It can be more specific if possible.


3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
A: Yes. The author practice data integrity. The author does not artificially make their results seem better. The author use the original data results from the experiment.
When analysing data, the author just use tool Rstudio to draw pictures to compare in an intuitive way and clearer.


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
A: The original data results are shown in separate text files with distinguishable file names. Then the author does draw some pictures using Rstudio, which is easier and
more intuitive for readers to tell the difference between compared data.


5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
A: From the experiment results, the author says SFQ does not have significant effect on default scheduling. However, this conclusion is based on just two bandwidths. We may add
a range for this conclusion that is when the bandwidths ranging from 1m to 100m, SFQ does not have significant effect on default scheduling. To get more general conclusion,
we can do more tests with more different bandwidths like 1000m and 10000m.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
A: Basically the instructions are clear and easy to follow. But in some detail, there maybe some problem the code like "ping –c 120 server > “file name” ". However, it is
easy to fix. In general, i would say it is a qualified instruction.


2) Were you able to successfully produce experiment results?
A: Yes, i successfully produce experiment results. The results compared to the author are close.

3) How long did it take you to run this experiment, from start to finish?
A: From start reading the whole report till I finish reproducing the experiment, it takes me about 5-6 hours. It is easy to understand the goal and procedure of this
experiment. It may come out some problem when doing iperf and Rstudio. But I can fix them. Generally, this is a mid-level difficulty experiment for me, and it does
not cost me too long.


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
A: I do not do big changes or add additional steps in doing this experiment. I just change some code. Like change "ping –c 120 server > “file name” "
to "ping –c 120 server -w “file name” ", change "iperf -s" to "iperf -s -u".


5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
A: Degree 3: The results can be reproduced by an independent researcher, requiring considerable effort.


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

The whole project is fine. I would suggest the report be more specific. For example, in the analysis and conlusion parts, the author can write more about how to tell the
difference between data results. In terms of conclusions, the author needs to be more accurate. Try to say in this way, under some conditions, we concludes some conclusions.
If the author wants to make more general conclusions, the author should take more experiments under different conditions.
