
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.
   The project is about to study the preformance of SFQ when small data flow competing with large data flow in the network.
   In the experiment, they fist generate large data flows bewteen the client and the server and then generate small data flows competing for the bandwidth. Then measure the latency of small data flow to see the performance of SFQ.


2) Does the project generally follow the guidelines and parameters we have
learned in class?
   Yes. The project roughly follows the guidelines and there is only one parameter in the experiment.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
   The goal in this experiment is to study the effect of SFQ under a situation where a small data flow is competing with a large data flow in a network.
   It is not a specific goal and it's not likely to have interesting results.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
   The metric is appropriate for this experiment goal, but the parameters are not.
   The design of experiment does not clearly support the experiment goal because it's hard to conclude whether SFQ has good performance or not when it only compares with the default queuing method. It should be also compared with other queuing methods.


3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
   Yes. In this experiment, they collect four sets of data for each situation which is fair and clear.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
    The chosen metric is clear enough to lead to the corrent conclusion.
	I can't come up with other metrics that are better suited for this experiment.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
   The parameter of the experiment is meaningful but only one parameter is apparently not enough.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
   No, the authors didn't address the possibility of interactions between parameters.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
   I don't think the comparison is reasonable enough to draw a conclusion.
   SFQ divides the traffic into a large number of FIFO queues. But the default queuing method also separates the traffic into FIFO queues. I would not be surprised if the results of these queuing methods are similar.


## Communicating results


1) Do the authors report the quantitative results of their experiment?
   Yes, the authors show the quantitative results in a folder.

2) Is there information given about the variation and/or distribution of
experimental results?
   The authors drew a plot showing the distribution of experimental results.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
   They just collected the data and plot pictures.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
   The data is presented in a clear and effective way by plotting pictures which are appropriate for the experiment.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
   The conclusion drawn by the authors is supported by the experiment results.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
   Yes, the authors include instructions in three ways and the instructions are clear and easy to understand.

2) Were you able to successfully produce experiment results?
   I'm able to produce results successfully.

3) How long did it take you to run this experiment, from start to finish?
   It took me about 25 minutes to run this experiment.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
   I don't need to make any changes.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
   I think this experiment should be degree 5 of reproducibility.  


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experimemt.

SFQ should be compared with other queuing methods, not just one, to see whether its performance is good or not.
