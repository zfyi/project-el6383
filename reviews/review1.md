
Project Review
=====================================================

## Overview

1) Briefly summarize the experiment in this project.
The experiment is to compare the performance of the application generating small data flows between network environments with and without SFQ.  
The difference between two network enviroments is the bandwidth, one is 1M and the other one is 100M.  
The application to generate small date flows here is ping.  
In this way, four experiment units are needed here:  
I.  bandwidth = 1M without SFQ
II. bandwidth = 1M with SFQ  
III.bandwidth = 100M without SFQ  
IV. bandwidth = 100M with SFQ  


2) Does the project generally follow the guidelines and parameters we have
learned in class?
The goal of this experiment is clear and pretty good. The matrix here is chosen as delay to indicate the performance.  
The parameters chosen here are bandwidth and SFQ status. The results are shown in pretty clear graphs.  

## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?  
Is it useful and likely to have interesting results?  
The goal of our project is to study the effect of SFQ under a situation where a small data flow is competing with a large data flow in a network.  
It is actually a specific goal. And I think the result here should be abvious that network environment perform better with SFQ than without SFQ  
under most conditions.  

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?  
The metric chosen here is delay and it's good to indicate the network performance.  
The parameters chosen here are bandwidth and SFQ status.  
The experiment design clearly support the experiment goal.  

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?  
I think that it only uses 4 experiment units to obtain the information and it can be said to be well designed.  

4) Are the metrics selected for study the *right* metrics? Are they clear,  
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
In my opinion, delay is the right metrics which is clear and unambiguous. I think that the goodput, the throughput or the packet loss rate can also  
be good choices to be used as the metrics to indicate the performance.  

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?  
The experiment parameters are meaningful. 100M is a raletive bandwidth and 1M is a relative small bandwidth. The scheduling method is with SFQ or
without SFQ.  

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
The autors here do not addressed the possibility of interactions between parameters.  


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?  
The comparisons made here is reasonably and the baseline selected here is appropriate.  


## Communicating results


1) Do the authors report the quantitative results of their experiment?  
Yes, the quantitative results are all in data file.  


2) Is there information given about the variation and/or distribution of
experimental results?
There is no information given about the variation and/or distribution of experimental results.  

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
The data here used the same base and there are no ratio games. The authors told the truth about their data.  

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
The data is presented in graphical form and there are three types of graphs to show us information the author wanted us to see.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?  
The experiment results support the conclusions drawn by the authors. But it seems not to be same with the original paper which shows
that network environment performs better with SFQ than that without SFQ.  

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear  
and easy to understand?  
Yes. The authors provided the original data which can give out the conclusion according to it, the experiment setup to collect the data and the material needed conduct the experiment.  
The instuctions are clear and easy to understand.  

2) Were you able to successfully produce experiment results?  
Yes, I follow the instuction step by step and successfully produce the experiment results.  

3) How long did it take you to run this experiment, from start to finish?
It takes me about half an hour to collect enough data I need. As to the date analysis, it takes me about 15 mins.  

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?  
I do not need any additional steps to complete this experiment.  

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?  
I think it falls on 4.  


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
